using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpRotation : MonoBehaviour
{
    [SerializeField] private Vector3[] _angles;
    [SerializeField] [Range(0f, 10f)] private float _lerpTime;
    [SerializeField] private float _time;

    private float _currentTime;
    private int _angleIndex;

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(_angles[_angleIndex]), _lerpTime * Time.deltaTime);

        if (_currentTime <= 0)
        {
            _currentTime = _time;
            _angleIndex++;
        }
        else
        {
            _currentTime -= Time.deltaTime;
        }

        if (_angleIndex >= _angles.Length)
        {
            _angleIndex = 0;
        }
    }
}

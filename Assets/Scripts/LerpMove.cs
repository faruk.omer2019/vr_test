using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpMove : MonoBehaviour
{
    [SerializeField] private Vector3[] _positions;
    [SerializeField] [Range(0f, 10f)] private float _lerpTime;
    [SerializeField] private float _time;

    private float _currentTime;
    private int _angleIndex;

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, _positions[_angleIndex], _lerpTime * Time.deltaTime);

        if (_currentTime <= 0)
        {
            _currentTime = _time;
            _angleIndex++;
        }
        else
        {
            _currentTime -= Time.deltaTime;
        }

        if (_angleIndex >= _positions.Length)
        {
            _angleIndex = 0;
        }
    }
}

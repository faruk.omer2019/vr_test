using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oscillator : MonoBehaviour
{
    float timeCounter = 0;
    public float speed;
    public float width;
    public float height;

    public float x, y, z;

    public float rx, ry, rz;

    public float offsetX, offsetY, offsetZ;

    // Start is called before the first frame update
    void Start()
    {
        /*speed = 5;
        width = 4;
        height = 7;
        x = this.transform.position.x;
        y = this.transform.position.y;
        z = this.transform.position.z;*/

    }

    // Update is called once per frame
    void Update()
    {
        timeCounter += Time.deltaTime * speed;

        x = Mathf.Cos(timeCounter) * width; 
        y = Mathf.Sin(timeCounter) * height;
        //z = Mathf.Sin(timeCounter) * height;
        this.transform.Rotate(rx, ry, rz);
        transform.position = new Vector3(x + offsetX, y + offsetY, z + offsetZ);
    }
}

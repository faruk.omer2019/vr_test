using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transition : MonoBehaviour
{
    public Material mat1, mat2, mat3, mat4;
    public GameObject spaceTerrain, spaceLighting, defaultLight, mainRoom, ocean, airport, lab, SAGE, land;

    // Start is called before the first frame update
    void Start()
    {

    }

    public void MainToSpace()
    {
        RenderSettings.skybox = mat1;
        defaultLight.SetActive(false);
        mainRoom.SetActive(false);
        ocean.SetActive(false);
        airport.SetActive(false);
        lab.SetActive(false);
        SAGE.SetActive(false);
        land.SetActive(false);
        spaceTerrain.SetActive(true);
        spaceLighting.SetActive(true);
    }

    public void SpaceToMain()
    {
        RenderSettings.skybox = mat2;
        spaceTerrain.SetActive(false);
        spaceLighting.SetActive(false);
        ocean.SetActive(false);
        airport.SetActive(false);
        lab.SetActive(false);
        SAGE.SetActive(false);
        land.SetActive(false);
        defaultLight.SetActive(true);
        mainRoom.SetActive(true);
    }

    public void ToOcean()
    {
        RenderSettings.skybox = mat3;
        spaceTerrain.SetActive(false);
        spaceLighting.SetActive(false);
        defaultLight.SetActive(false);
        mainRoom.SetActive(false);
        airport.SetActive(false);
        lab.SetActive(false);
        SAGE.SetActive(false);
        land.SetActive(false);
        ocean.SetActive(true);
    }

    public void ToAir()
    {
        RenderSettings.skybox = mat4;
        spaceTerrain.SetActive(false);
        spaceLighting.SetActive(false);
        defaultLight.SetActive(false);
        mainRoom.SetActive(false);
        ocean.SetActive(false);
        lab.SetActive(false);
        SAGE.SetActive(false);
        land.SetActive(false);
        airport.SetActive(true);
    }

    public void ToLab()
    {
        RenderSettings.skybox = mat4;
        spaceTerrain.SetActive(false);
        spaceLighting.SetActive(false);
        defaultLight.SetActive(false);
        mainRoom.SetActive(false);
        ocean.SetActive(false);
        airport.SetActive(false);
        SAGE.SetActive(false);
        land.SetActive(false);
        lab.SetActive(true);
    }

    public void ToSAGE()
    {
        RenderSettings.skybox = mat4;
        spaceTerrain.SetActive(false);
        spaceLighting.SetActive(false);
        defaultLight.SetActive(false);
        mainRoom.SetActive(false);
        ocean.SetActive(false);
        airport.SetActive(false);
        lab.SetActive(false);
        land.SetActive(false);
        SAGE.SetActive(true);
    }

    public void ToLand()
    {
        RenderSettings.skybox = mat4;
        spaceTerrain.SetActive(false);
        spaceLighting.SetActive(false);
        defaultLight.SetActive(false);
        mainRoom.SetActive(false);
        ocean.SetActive(false);
        airport.SetActive(false);
        lab.SetActive(false);
        SAGE.SetActive(false);
        land.SetActive(true);
    }

}
